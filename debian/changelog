libconvert-nls-date-format-perl (0.06-2) UNRELEASED; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 30 Jan 2016 20:03:35 +0100

libconvert-nls-date-format-perl (0.06-1) unstable; urgency=medium

  * New upstream release.
  * Drop escape-brace-in-regex.patch, merged upstream.
  * Update years of upstream and packaging copyright.
  * Update build dependencies.

 -- gregor herrmann <gregoa@debian.org>  Fri, 22 Jan 2016 21:57:23 +0100

libconvert-nls-date-format-perl (0.05-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * Escape brace in regexp in Convert::NLS_DATE_FORMAT.
    Unescaped left brace used in regexp is deprecated and issues a warning
    under Perl 5.22. (Closes: #809093)
  * Bump Debhelper compat level to 9
  * Declare compliance with Debian policy 3.9.6
  * Declare package as autopkgtestable

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 27 Dec 2015 12:59:23 +0100

libconvert-nls-date-format-perl (0.05-1) unstable; urgency=low

  * New upstream release.
  * Bump years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Wed, 18 Jan 2012 19:16:09 +0100

libconvert-nls-date-format-perl (0.03-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream and packaging copyright.
  * Set Standards-Version to 3.9.2 (no changes).
  * Bump debhelper compatibility level to 8.

 -- gregor herrmann <gregoa@debian.org>  Sat, 15 Oct 2011 21:11:22 +0200

libconvert-nls-date-format-perl (0.02-1) unstable; urgency=low

  * Initial release (closes: #603474).

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Nov 2010 16:04:25 +0100
